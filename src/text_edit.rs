use std::f32::consts::PI;
use std::time;

use eframe::egui::{
    self, style,
    text_selection::{self as txtsel, text_cursor_state, visuals},
    widgets::Widget,
};
use epaint::text::cursor as egui_cursor;
use scribe::buffer as sbuf;
use syntect::{self as syn, highlighting as hl, parsing as pars};

use crate::buffer::Buffer;
use crate::extensions::ContextExtension;

// The cursor blink time (period) in multiples of animation_time.
const CURSOR_BLINK_TIME_FACTOR: f32 = 10.0;

// How long the cursor remains steady after moving, in multiples of
// animation_time.
const CURSOR_STEADY_TIME_FACTOR: f32 = 10.0;

pub struct TextEdit<'b, 's, 't> {
    id: egui::Id,
    buffer: &'b mut Buffer,
    syntax_set: &'s pars::SyntaxSet,
    theme: &'t hl::Theme,
    margin: egui::Vec2,
}

impl<'b, 's, 't> TextEdit<'b, 's, 't> {
    pub fn new(
        id: egui::Id,
        buffer: &'b mut Buffer,
        syntax_set: &'s pars::SyntaxSet,
        theme: &'t hl::Theme,
    ) -> TextEdit<'b, 's, 't> {
        TextEdit { id, buffer, syntax_set, theme, margin: egui::vec2(4.0, 2.0) }
    }
}

impl<'b, 's, 't> Widget for TextEdit<'b, 's, 't> {
    fn ui(mut self, ui: &mut egui::Ui) -> egui::Response {
        let font_id = style::TextStyle::Monospace.resolve(ui.style());

        let background = self
            .theme
            .settings
            .background
            .map(|c| color32_from_color(c))
            .unwrap_or_else(|| ui.visuals().extreme_bg_color);

        let wrap_width = ui.available_size().x - 2.0 * self.margin.x;

        // Highlight text
        let mut layout_job =
            egui::text::LayoutJob::simple(String::new(), font_id.clone(), background, wrap_width);
        if let Some(ref syntax) = self.buffer.syntax_definition() {
            let mut highlighter = syn::easy::HighlightLines::new(syntax, &self.theme);
            for line in syn::util::LinesWithEndings::from(&self.buffer.data()) {
                if let Ok(ranges) = highlighter.highlight_line(line, &self.syntax_set) {
                    for (style, text) in ranges {
                        layout_job.append(
                            text,
                            0.0,
                            egui_text_format_from_syntect_style(&style, font_id.clone()),
                        );
                    }
                }
            }
        }

        let galley = ui.fonts(|fonts| fonts.layout_job(layout_job));

        let (mut response, painter) =
            ui.allocate_painter(galley.rect.size() + 2.0 * self.margin, egui::Sense::hover());

        response.id = self.id;
        response.widget_info(|| egui::WidgetInfo::new(egui::WidgetType::Label));
        let sense = egui::Sense { click: true, drag: false, focusable: true };
        response |= ui.interact(response.rect, self.id, sense);

        let widget_visuals = ui.style().interact(&response);
        paint_background(&painter, ui.visuals(), widget_visuals, background, &response);

        // Paint text
        let content_origin = response.rect.left_top() + self.margin;
        let fill_color = ui.visuals().extreme_bg_color;
        painter.galley(content_origin, galley.clone(), fill_color);

        // Paint selection
        let egui_cursor_range = txtsel::CursorRange::two(
            egui_cursor_from_cursor(&galley, self.buffer.cursor()),
            egui_cursor_from_cursor(&galley, self.buffer.secondary_cursor()),
        );
        visuals::paint_text_selection(
            &painter,
            &ui.visuals(),
            content_origin,
            &galley,
            &egui_cursor_range,
            None,
        );

        // Paint cursor
        let egui_cursor = egui_cursor_from_cursor(&galley, self.buffer.cursor());
        let row_height = ui.fonts(|fonts| fonts.row_height(&font_id));
        let cursor_rect =
            text_cursor_state::cursor_rect(content_origin, &*galley, &egui_cursor, row_height);
        let shrunken_cursor_rect = scale_rect_vertically(
            &cursor_rect,
            animate_cursor_size(ui.ctx(), self.buffer.cursor()),
        );
        visuals::paint_cursor(&painter, ui.visuals(), shrunken_cursor_rect);

        if ui.memory(|mem| mem.has_focus(self.id)) {
            ui.input(|input| self.handle_events(&input.events, &galley, content_origin));
        }
        response
    }
}

fn egui_text_format_from_syntect_style(
    style: &hl::Style,
    font_id: egui::FontId,
) -> egui::text::TextFormat {
    egui::text::TextFormat {
        font_id,
        extra_letter_spacing: 0.0,
        line_height: None,
        color: color32_from_color(style.foreground),
        background: color32_from_color(style.background),
        italics: style.font_style & hl::FontStyle::ITALIC != hl::FontStyle::empty(),
        underline: if style.font_style & hl::FontStyle::UNDERLINE != hl::FontStyle::empty() {
            egui::Stroke { width: 1.0, color: color32_from_color(style.foreground) }
        } else {
            egui::Stroke::NONE
        },
        strikethrough: egui::Stroke::NONE,
        valign: egui::Align::Max,
    }
}

fn color32_from_color(color: hl::Color) -> egui::Color32 {
    egui::Color32::from_rgba_unmultiplied(color.r, color.g, color.b, color.a)
}

impl<'b, 's, 't> TextEdit<'b, 's, 't> {
    fn handle_events(&mut self, events: &Vec<egui::Event>, galley: &egui::Galley, galley_origin: egui::Pos2) {
        for event in events {
            match event {
                egui::Event::Text(text_to_insert) => {
                    self.buffer.insert(text_to_insert);
                    self.buffer.cursor_mut().move_right();
                    self.buffer.set_secondary_cursor_to_primary_cursor();
                }
                egui::Event::Key { key: egui::Key::Enter, pressed: true, .. } => {
                    self.buffer.insert('\n');
                    let cursor = self.buffer.cursor_mut();
                    cursor.move_to(sbuf::Position { line: cursor.position.line + 1, offset: 0 });
                    self.buffer.set_secondary_cursor_to_primary_cursor();
                }
                egui::Event::Key { key: egui::Key::Backspace, pressed: true, .. } => {
                    let mut prev_char_cursor = self.buffer.cursor().clone();
                    if prev_char_cursor.position.offset > 0 {
                        prev_char_cursor.move_left();
                    } else if prev_char_cursor.position.line > 0 {
                        prev_char_cursor.position.line -= 1;
                        prev_char_cursor.move_to_end_of_line();
                    }
                    if prev_char_cursor.position != self.buffer.cursor().position {
                        self.buffer.delete_range(sbuf::Range::new(
                            prev_char_cursor.position,
                            self.buffer.cursor().position,
                        ));
                        self.buffer.cursor_mut().move_to(prev_char_cursor.position);
                        self.buffer.set_secondary_cursor_to_primary_cursor();
                    }
                }
                egui::Event::Key { key: egui::Key::Delete, pressed: true, .. } => {
                    self.buffer.delete();
                    self.buffer.set_secondary_cursor_to_primary_cursor();
                }
                egui::Event::Key {
                    key: egui::Key::ArrowLeft,
                    pressed: true,
                    modifiers: egui::Modifiers::NONE,
                    ..
                } => {
                    self.buffer.cursor_mut().move_left();
                    self.buffer.set_secondary_cursor_to_primary_cursor();
                }
                egui::Event::Key {
                    key: egui::Key::ArrowRight,
                    pressed: true,
                    modifiers: egui::Modifiers::NONE,
                    ..
                } => {
                    self.buffer.cursor_mut().move_right();
                    self.buffer.set_secondary_cursor_to_primary_cursor();
                }
                egui::Event::Key {
                    key: egui::Key::ArrowUp,
                    pressed: true,
                    modifiers: egui::Modifiers::NONE,
                    ..
                } => {
                    self.buffer.cursor_mut().move_up();
                    self.buffer.set_secondary_cursor_to_primary_cursor();
                }
                egui::Event::Key {
                    key: egui::Key::ArrowDown,
                    pressed: true,
                    modifiers: egui::Modifiers::NONE,
                    ..
                } => {
                    self.buffer.cursor_mut().move_down();
                    self.buffer.set_secondary_cursor_to_primary_cursor();
                }
                egui::Event::PointerButton {
                    pos,
                    button: egui::PointerButton::Primary,
                    pressed: true,
                    modifiers: egui::Modifiers::NONE,
                } => {
                    let egui_cursor = galley.cursor_from_pos(*pos - galley_origin);
                    let position = position_from_egui_cursor(&egui_cursor);
                    self.buffer.cursor_mut().move_to(position);
                    self.buffer.set_secondary_cursor_to_primary_cursor();
                }
                _ => {}
            }
        }
    }
}

fn paint_background(
    painter: &egui::Painter,
    visuals: &egui::Visuals,
    widget_visuals: &style::WidgetVisuals,
    fill_color: egui::Color32,
    response: &egui::Response,
) {
    let frame_color =
        if response.has_focus() { visuals.selection.stroke } else { widget_visuals.bg_stroke };
    painter.rect(response.rect, widget_visuals.rounding, fill_color, frame_color);
}

fn egui_cursor_from_cursor(galley: &egui::Galley, cursor: &sbuf::Cursor) -> egui_cursor::Cursor {
    galley.from_pcursor(egui_pcursor_from_cursor(cursor))
}

fn egui_pcursor_from_cursor(cursor: &sbuf::Cursor) -> egui_cursor::PCursor {
    egui_cursor::PCursor { paragraph: cursor.line, offset: cursor.offset, prefer_next_row: true }
}

fn position_from_egui_cursor(egui_cursor: &egui_cursor::Cursor) -> sbuf::Position {
    sbuf::Position { line: egui_cursor.pcursor.paragraph, offset: egui_cursor.pcursor.offset }
}

fn check_for_cursor_movement(ctx: &egui::Context, cursor: &sbuf::Cursor) -> bool {
    let id = egui::Id::new("prev_cursor_position");
    let result = if let Some(prev_position) = ctx.get_temp::<sbuf::Position>(id) {
        cursor.position != prev_position
    } else {
        false
    };
    ctx.insert_temp(id, cursor.position);
    result
}

fn animate_cursor_size(ctx: &egui::Context, cursor: &sbuf::Cursor) -> f32 {
    if ctx.input(|i| !i.focused) {
        return 1.0;
    }

    let animation_start_id = egui::Id::new("cursor_animation_start");

    let animation_time = ctx.style().animation_time;
    let blink_time = CURSOR_BLINK_TIME_FACTOR * animation_time;
    let steady_time = CURSOR_STEADY_TIME_FACTOR * animation_time;
    let now = time::Instant::now();
    let cursor_did_move = check_for_cursor_movement(ctx, &cursor);

    if cursor_did_move {
        let next_start_time = now + time::Duration::from_secs_f32(steady_time);
        ctx.insert_temp(animation_start_id, next_start_time);
    }

    let start_time = ctx.data_mut(|d| d.get_temp_mut_or(animation_start_id, now).clone());
    if start_time > now {
        ctx.request_repaint_after(start_time - now);
        1.0
    } else {
        // The phase of the cyclic animation, from 0.0 to 1.0.
        let phase = (now - start_time).as_secs_f32() / blink_time;
        if phase > 1.0 {
            ctx.insert_temp(animation_start_id, now);
        }
        ctx.request_repaint();
        (PI * phase).cos().abs()
    }
}

fn scale_rect_vertically(rect: &egui::Rect, factor: f32) -> egui::Rect {
    rect.shrink2(egui::vec2(1.0, (1.0 - factor) * rect.height() / 2.0))
}
