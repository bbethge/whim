use std::array;
use std::iter;
use std::ops;
use std::slice;

pub const MENU_ROWS: usize = 3;
pub const MENU_COLUMNS: usize = 10;

pub struct MenuArray<T>([[T; MENU_COLUMNS]; MENU_ROWS]);

impl<T> MenuArray<T> {
    pub const fn new(rows: [[T; MENU_COLUMNS]; MENU_ROWS]) -> MenuArray<T> {
        MenuArray(rows)
    }

    pub fn map<U>(self, mut function: impl FnMut(T) -> U) -> MenuArray<U> {
        let mut items: [[Option<U>; MENU_COLUMNS]; MENU_ROWS] = Default::default();
        for (row_index, row) in self.0.into_iter().enumerate() {
            for (column_index, item) in row.into_iter().enumerate() {
                items[row_index][column_index] = Some(function(item));
            }
        }
        MenuArray(items).unwrap()
    }

    pub fn as_ref(&self) -> MenuArray<&T> {
        let mut refs: MenuArray<Option<&T>> = Default::default();
        for (row_index, row) in (&self.0).into_iter().enumerate() {
            for (column_index, item) in row.iter().enumerate() {
                refs.0[row_index][column_index] = Some(item);
            }
        }
        refs.unwrap()
    }

    pub fn zip<U>(self, other: MenuArray<U>) -> MenuArray<(T, U)> {
        let mut items: [[Option<(T, U)>; MENU_COLUMNS]; MENU_ROWS] = Default::default();
        for (row_index, (row0, row1)) in self.0.into_iter().zip(other.0.into_iter()).enumerate() {
            for (column_index, (item0, item1)) in row0.into_iter().zip(row1.into_iter()).enumerate()
            {
                items[row_index][column_index] = Some((item0, item1));
            }
        }
        MenuArray(items).unwrap()
    }

    pub fn rows(&self) -> Iter<'_, T> {
        (&self.0).into_iter()
    }

    pub fn select(&self, filter: MenuArray<bool>) -> Select<'_, T> {
        self.as_ref().zip(filter).into_iter().flatten().filter_map(wrap_if)
    }
}

impl<T> MenuArray<Option<T>> {
    fn unwrap(self) -> MenuArray<T> {
        MenuArray(self.0.map(|row| row.map(Option::unwrap)))
    }
}

pub type Iter<'a, T> = slice::Iter<'a, [T; MENU_COLUMNS]>;

pub type Select<'a, T> = iter::FilterMap<
    iter::Flatten<<MenuArray<(&'a T, bool)> as IntoIterator>::IntoIter>,
    fn((&'a T, bool)) -> Option<&'a T>,
>;

fn wrap_if<T>((inner, condition): (T, bool)) -> Option<T> {
    if condition {
        Some(inner)
    } else {
        None
    }
}

impl<T> Default for MenuArray<T>
where
    T: Default,
{
    fn default() -> MenuArray<T> {
        MenuArray(Default::default())
    }
}

impl<T> ops::Index<usize> for MenuArray<T> {
    type Output = [T; MENU_COLUMNS];

    fn index(&self, row: usize) -> &[T; MENU_COLUMNS] {
        &self.0[row]
    }
}

impl<T> ops::IndexMut<usize> for MenuArray<T> {
    fn index_mut(&mut self, row: usize) -> &mut [T; MENU_COLUMNS] {
        &mut self.0[row]
    }
}

impl<T> IntoIterator for MenuArray<T> {
    type Item = [T; MENU_COLUMNS];
    type IntoIter = array::IntoIter<[T; MENU_COLUMNS], MENU_ROWS>;

    fn into_iter(self) -> Self::IntoIter {
        self.0.into_iter()
    }
}

impl MenuArray<bool> {
    pub fn any(&self) -> bool {
        self.0.iter().any(|row| row.iter().any(|b| *b))
    }
}
