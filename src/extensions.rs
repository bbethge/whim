use std::any;
use std::time;

use eframe::egui;
use fancy_regex as re;
use scribe::buffer as sbuf;
use unicode_general_category as ugc;
use unicode_segmentation::UnicodeSegmentation;

pub trait CursorExtension {
    fn start_of_current_line(&self) -> sbuf::Position;
    fn end_of_current_line(&self) -> sbuf::Position;
    fn current_line(&self) -> String;
    fn preceding_part_of_current_line(&self) -> String;
    fn following_part_of_current_line(&self) -> String;
    fn move_to_previous_word(&mut self);
    fn move_to_next_word(&mut self);
    fn move_right_by(&mut self, offset: usize);
    fn move_to_start_of_next_line(&mut self) -> bool;
    fn search(&mut self, regex: &re::Regex) -> bool;
    fn is_at_start_of_line(&self) -> bool;
    fn is_at_end_of_line(&self) -> bool;
    fn is_on_first_line(&self) -> bool;
    fn is_on_last_line(&self) -> bool;
}

impl CursorExtension for sbuf::Cursor {
    fn start_of_current_line(&self) -> sbuf::Position {
        let mut start_of_line_cursor = self.clone();
        start_of_line_cursor.move_to_start_of_line();
        start_of_line_cursor.position
    }

    fn end_of_current_line(&self) -> sbuf::Position {
        let mut end_of_line_cursor = self.clone();
        end_of_line_cursor.move_to_end_of_line();
        end_of_line_cursor.position
    }

    fn current_line(&self) -> String {
        self.data
            .borrow()
            .read(&sbuf::Range::new(self.start_of_current_line(), self.end_of_current_line()))
            .unwrap_or("".to_owned())
    }

    fn preceding_part_of_current_line(&self) -> String {
        self.data
            .borrow()
            .read(&sbuf::Range::new(self.start_of_current_line(), self.position))
            .unwrap_or("".to_owned())
    }

    fn following_part_of_current_line(&self) -> String {
        self.data
            .borrow()
            .read(&sbuf::Range::new(self.position, self.end_of_current_line()))
            .unwrap_or("".to_owned())
    }

    fn move_to_previous_word(&mut self) {
        let first_part_of_line = self.preceding_part_of_current_line();
        let offset_to_previous_word = self
            .position
            .offset
            .saturating_sub(first_part_of_line.reverse_char_offset_of_last_word());
        self.move_to(sbuf::Position { line: self.position.line, offset: offset_to_previous_word });
    }

    fn move_to_next_word(&mut self) {
        let rest_of_line = self.following_part_of_current_line();
        let distance_to_next_word =
            sbuf::Distance { lines: 0, offset: rest_of_line.char_offset_of_next_word() };
        self.move_to(self.position + distance_to_next_word);
    }

    fn move_right_by(&mut self, offset: usize) {
        self.move_to(self.position + sbuf::Distance { lines: 0, offset });
    }

    fn move_to_start_of_next_line(&mut self) -> bool {
        let initial_line = self.position.line;
        self.move_to(sbuf::Position { line: initial_line + 1, offset: 0 });
        self.position.line > initial_line
    }

    fn search(&mut self, regex: &re::Regex) -> bool {
        // Advance ‘self’ to the start of the next match of ‘regex’, or
        // leave unchanged if there is no match.  Return true if there
        // was a match.
        let mut cursor = self.clone();
        while !search_current_line(&mut cursor, regex) {
            if !cursor.move_to_start_of_next_line() {
                return false;
            }
        }
        self.clone_from(&cursor);
        true
    }

    fn is_at_start_of_line(&self) -> bool {
        self.position.offset == 0
    }

    fn is_at_end_of_line(&self) -> bool {
        let mut test_cursor = self.clone();
        // move_to returns true if the position is in bounds
        !test_cursor.move_to(test_cursor.position + sbuf::Distance { lines: 0, offset: 1 })
    }

    fn is_on_first_line(&self) -> bool {
        self.position.line == 0
    }

    fn is_on_last_line(&self) -> bool {
        let mut test_cursor = self.clone();
        // move_to returns true if the position is in bounds
        !test_cursor.move_to(sbuf::Position { line: test_cursor.position.line + 1, offset: 0 })
    }
}

fn search_current_line(cursor: &mut sbuf::Cursor, regex: &re::Regex) -> bool {
    // Advance ‘cursor’ to the start of the next match of ‘regex’ on
    // the current line, or leave unchanged if there is no match.
    // Return true if there was a match.
    let text_to_search = cursor.following_part_of_current_line();
    if let Ok(Some(regex_match)) = regex.find(&text_to_search) {
        let offset_to_start_of_match = grapheme_offset_of_match(&text_to_search, &regex_match);
        cursor.move_right_by(offset_to_start_of_match);
        return true;
    }
    false
}

fn grapheme_offset_of_match(searched_text: &str, regex_match: &re::Match) -> usize {
    byte_offset_to_grapheme_offset(&searched_text, regex_match.start())
        .expect("Regex match byte indices should be valid")
}

// Convert a byte offset into a string to a grapheme offset.
fn byte_offset_to_grapheme_offset(text: &str, byte_offset: usize) -> Option<usize> {
    let mut grapheme_offset: usize = 0;

    // Look for a grapheme with a matching byte offset
    for (grapheme_byte_offset, _) in text.grapheme_indices(true) {
        if grapheme_byte_offset == byte_offset {
            return Some(grapheme_offset);
        }
        grapheme_offset += 1;
    }

    // Maybe the byte offset is at the end of the string
    if text.len() == byte_offset {
        // Then return the number of graphemes in the string
        Some(grapheme_offset)
    } else {
        // Invalid byte offset
        None
    }
}

pub trait StringExtension {
    fn starts_with_unicode_space(self) -> bool;
    fn char_count(self) -> usize;
    fn char_offset_of_next_word(self) -> usize;
    fn reverse_char_offset_of_last_word(self) -> usize;
}

impl StringExtension for &str {
    fn starts_with_unicode_space(self) -> bool {
        if let Some(first_char) = self.chars().next() {
            first_char.is_unicode_space()
        } else {
            false
        }
    }

    fn char_count(self) -> usize {
        self.chars().count()
    }

    fn char_offset_of_next_word(self) -> usize {
        let mut offset = 0;
        for part in self.split_word_bounds() {
            if offset == 0 || part.starts_with_unicode_space() {
                offset += part.char_count();
            } else {
                break;
            }
        }
        if offset == 0 {
            offset = self.char_count();
        }
        offset
    }

    fn reverse_char_offset_of_last_word(self) -> usize {
        let mut offset = 0;
        for part in self.split_word_bounds().rev() {
            offset += part.char_count();
            if !part.starts_with_unicode_space() {
                break;
            }
        }
        offset
    }
}

pub trait IsUnicodeSpace {
    fn is_unicode_space(self) -> bool;
}

impl IsUnicodeSpace for char {
    fn is_unicode_space(self) -> bool {
        ugc::get_general_category(self) == ugc::GeneralCategory::SpaceSeparator
    }
}

pub trait ContextExtension {
    // Return a bool that stays true for Style::animation_time seconds
    // after each time should_trigger_pulse is true.
    fn animate_bool_pulse(&self, id: egui::Id, should_trigger_pulse: bool) -> bool;

    fn get_temp<T: 'static + Clone>(&self, id: egui::Id) -> Option<T>;

    fn insert_temp<T>(&self, id: egui::Id, value: T)
    where
        T: 'static + any::Any + Clone + Send + Sync;
}

impl ContextExtension for egui::Context {
    fn animate_bool_pulse(&self, id: egui::Id, should_trigger_pulse: bool) -> bool {
        let animation_time = time::Duration::from_secs_f32(self.style().animation_time);
        let now = time::Instant::now();

        if should_trigger_pulse {
            self.data_mut(|data| data.insert_temp(id, now));
        }

        if let Some(last_triggered_time) = self.data(|data| data.get_temp::<time::Instant>(id)) {
            let time_since_triggered = now - last_triggered_time;
            if time_since_triggered < animation_time {
                self.request_repaint_after(animation_time - time_since_triggered);
            }
            time_since_triggered < animation_time
        } else {
            false
        }
    }

    fn get_temp<T: 'static + Clone>(&self, id: egui::Id) -> Option<T> {
        self.data(|data| data.get_temp::<T>(id))
    }

    fn insert_temp<T>(&self, id: egui::Id, value: T)
    where
        T: 'static + any::Any + Clone + Send + Sync,
    {
        self.data_mut(|data| data.insert_temp(id, value));
    }
}
