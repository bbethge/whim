use std::io;
use std::path;

use scribe::buffer as sbuf;
use syntect::parsing as pars;

use crate::extensions::CursorExtension;

pub struct Buffer {
    inner: scribe::Buffer,
    secondary_cursor: scribe::buffer::Cursor,
}

impl Buffer {
    pub fn path(&self) -> Option<&path::PathBuf> {
        self.inner.path.as_ref()
    }

    pub fn cursor(&self) -> &sbuf::Cursor {
        &self.inner.cursor
    }

    pub fn cursor_mut(&mut self) -> &mut sbuf::Cursor {
        &mut self.inner.cursor
    }

    pub fn secondary_cursor(&self) -> &sbuf::Cursor {
        &self.secondary_cursor
    }

    pub fn secondary_cursor_mut(&mut self) -> &mut sbuf::Cursor {
        &mut self.secondary_cursor
    }

    pub fn set_secondary_cursor_to_primary_cursor(&mut self) {
        self.secondary_cursor.clone_from(&self.inner.cursor);
    }

    pub fn syntax_definition(&self) -> &Option<pars::SyntaxReference> {
        &self.inner.syntax_definition
    }

    pub fn syntax_definition_mut(&mut self) -> &mut Option<pars::SyntaxReference> {
        &mut self.inner.syntax_definition
    }

    pub fn new() -> Buffer {
        let inner = sbuf::Buffer::new();
        let secondary_cursor = inner.cursor.clone();
        Buffer { inner, secondary_cursor }
    }

    pub fn from_file(path: &path::Path) -> io::Result<Buffer> {
        let inner = sbuf::Buffer::from_file(path)?;
        let secondary_cursor = inner.cursor.clone();
        Ok(Buffer { inner, secondary_cursor })
    }

    pub fn save(&mut self) -> io::Result<()> {
        self.inner.save()
    }

    pub fn data(&self) -> String {
        self.inner.data()
    }

    pub fn collapse_cursors_left(&mut self) {
        let position = position_min(self.inner.cursor.position, self.secondary_cursor.position);
        self.set_both_cursors_to_position(position);
    }

    pub fn collapse_cursors_right(&mut self) {
        let position = position_max(self.inner.cursor.position, self.secondary_cursor.position);
        self.set_both_cursors_to_position(position);
    }

    pub fn set_both_cursors_to_position(&mut self, position: sbuf::Position) {
        self.inner.cursor.move_to(position);
        self.secondary_cursor.move_to(position);
    }

    pub fn has_selection(&self) -> bool {
        self.inner.cursor.position != self.secondary_cursor.position
    }

    pub fn selection_range(&self) -> sbuf::Range {
        sbuf::Range::new(self.inner.cursor.position, self.secondary_cursor.position)
    }

    pub fn delete(&mut self) {
        self.inner.delete();
    }

    pub fn delete_selection(&mut self) {
        self.inner.delete_range(self.selection_range());
        self.collapse_cursors_left();
    }

    pub fn delete_range(&mut self, range: sbuf::Range) {
        self.inner.delete_range(range);
    }

    pub fn insert<T: Into<String>>(&mut self, text: T) {
        if self.has_selection() {
            self.delete_selection();
        }
        self.inner.insert(text);
    }

    pub fn read_selection(&self) -> Option<String> {
        if self.has_selection() {
            self.inner.read(&self.selection_range())
        } else {
            None
        }
    }

    pub fn read_current_line(&self) -> String {
        self.inner
            .read(&sbuf::Range::new(
                self.inner.cursor.start_of_current_line(),
                self.inner.cursor.end_of_current_line(),
            ))
            .expect("The cursor should be on a valid line")
    }

    pub fn offset_of_first_non_blank_on_current_line(&self) -> usize {
        let line = self.read_current_line();
        // This is both a byte and a grapheme offset, because it only
        // skips over ASCII space and tab, which are 1-byte graphemes.
        line.find(|ch| ch != ' ' && ch != '\t').unwrap_or_else(|| line.len())
    }

    pub fn start_operation_group(&mut self) {
        self.inner.start_operation_group();
    }

    pub fn end_operation_group(&mut self) {
        self.inner.end_operation_group();
    }

    pub fn modified(&self) -> bool {
        self.inner.modified()
    }

    pub fn undo(&mut self) {
        self.inner.undo();
    }

    pub fn redo(&mut self) {
        self.inner.redo();
    }
}

// Position implements PartialOrd but not Ord even though the
// implementation is a total order.  We can’t just
// `impl Ord for Position {}`, so we make our own min/max functions
// instead.
fn position_min(position0: sbuf::Position, position1: sbuf::Position) -> sbuf::Position {
    if position0 <= position1 {
        position0
    } else {
        position1
    }
}

fn position_max(position0: sbuf::Position, position1: sbuf::Position) -> sbuf::Position {
    if position0 >= position1 {
        position0
    } else {
        position1
    }
}
struct Lines<'b> {
    scribe_buffer: &'b sbuf::Buffer,
    start_cursor: Option<sbuf::Cursor>,
}

impl<'b> Iterator for Lines<'b> {
    type Item = String;

    fn next(&mut self) -> Option<String> {
        if let Some(mut start_cursor) = self.start_cursor.clone() {
            let mut end_cursor = start_cursor.clone();
            end_cursor.move_to_end_of_line();
            let result = self
                .scribe_buffer
                .read(&sbuf::Range::new(start_cursor.position, end_cursor.position))
                .unwrap();
            if start_cursor
                .move_to(sbuf::Position { line: start_cursor.position.line + 1, offset: 0 })
            {
                self.start_cursor = Some(start_cursor);
            } else {
                self.start_cursor = None
            }
            Some(result)
        } else {
            None
        }
    }
}
