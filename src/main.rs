use std::collections::HashMap;
use std::env;
use std::path::Path;
use std::sync::Arc;

use eframe::egui;
use egui_winit::clipboard::Clipboard;
use fancy_regex as re;
use raw_window_handle::HasDisplayHandle;
use scribe::buffer as buf;
use syntect::{highlighting as hl, parsing as pars};

mod extensions;
use extensions::CursorExtension;
use extensions::ContextExtension;

mod menu_array;
use menu_array::{MenuArray, MENU_COLUMNS};

mod text_edit;
use text_edit as txted;

mod buffer;
use buffer::Buffer;

fn main() -> eframe::Result<()> {
    let options = eframe::NativeOptions {
        viewport: egui::ViewportBuilder::default().with_inner_size([768.0, 512.0]),
        ..Default::default()
    };
    let mut args = env::args();
    assert!(args.next().is_some());
    let filename_to_open = args.next();
    eframe::run_native(
        &format_window_title(filename_to_open.as_deref()),
        options,
        Box::new(|creation_context| {
            let clipboard = get_clipboard_from_creation_context(creation_context);
            Box::new(MyApp::new(clipboard, filename_to_open))
        }),
    )
}

fn format_window_title(filename: Option<&str>) -> String {
    if let Some(filename) = filename {
        format!("{filename} – Scriggle")
    } else {
        "Scriggle".to_owned()
    }
}

fn get_clipboard_from_creation_context(creation_context: &eframe::CreationContext) -> Clipboard {
    let raw_display_handle = creation_context.display_handle().map(|h| h.as_raw());
    if let Err(ref error) = raw_display_handle {
        println!("{:?}", error);
    }
    Clipboard::new(raw_display_handle.ok())
}

struct MyApp {
    error_message: Option<String>,
    buffer: Buffer,
    syntax_set: pars::SyntaxSet,
    theme_set: hl::ThemeSet,
    menu_stack: Vec<&'static Menu>,
    search_state: Option<SearchState>,
    key_locations: HashMap<char, (usize, usize)>,
    // To paste programmatically, we need a Clipboard.
    clipboard: Clipboard,
}

struct SearchState {
    pattern: String,
    cursor_to_move: CursorType,
}

enum CursorType {
    Both,
    Secondary,
}

impl MyApp {
    fn new(clipboard: Clipboard, filename_to_open: Option<String>) -> MyApp {
        let (mut buffer, error_message) = if let Some(ref filename_to_open) = filename_to_open {
            match Buffer::from_file(Path::new(filename_to_open)) {
                Ok(buffer) => (buffer, None),
                Err(error) => {
                    let error_message =
                        format!("Failed to load {}: {}", filename_to_open, error.to_string());
                    (Buffer::new(), Some(error_message))
                }
            }
        } else {
            (Buffer::new(), None)
        };
        let syntax_set = pars::SyntaxSet::load_defaults_newlines();
        if let Some(syntax) =
            buffer.path().and_then(|path| syntax_set.find_syntax_for_file(path).ok())
        {
            *buffer.syntax_definition_mut() = syntax.cloned();
        } else {
            *buffer.syntax_definition_mut() = Some(syntax_set.find_syntax_plain_text().clone());
        }

        MyApp {
            error_message,
            buffer,
            syntax_set,
            theme_set: hl::ThemeSet::load_defaults(),
            menu_stack: vec![&MAIN_MENU],
            search_state: None,
            key_locations: key_locations_us_qwerty(),
            clipboard,
        }
    }
}

fn key_locations_us_qwerty() -> HashMap<char, (usize, usize)> {
    let mut result = HashMap::new();

    result.insert('q', (0, 0));
    result.insert('w', (0, 1));
    result.insert('e', (0, 2));
    result.insert('r', (0, 3));
    result.insert('t', (0, 4));
    result.insert('y', (0, 5));
    result.insert('u', (0, 6));
    result.insert('i', (0, 7));
    result.insert('o', (0, 8));
    result.insert('p', (0, 9));

    result.insert('a', (1, 0));
    result.insert('s', (1, 1));
    result.insert('d', (1, 2));
    result.insert('f', (1, 3));
    result.insert('g', (1, 4));
    result.insert('h', (1, 5));
    result.insert('j', (1, 6));
    result.insert('k', (1, 7));
    result.insert('l', (1, 8));
    result.insert(';', (1, 9));

    result.insert('z', (2, 0));
    result.insert('x', (2, 1));
    result.insert('c', (2, 2));
    result.insert('v', (2, 3));
    result.insert('b', (2, 4));
    result.insert('n', (2, 5));
    result.insert('m', (2, 6));
    result.insert(',', (2, 7));
    result.insert('.', (2, 8));
    result.insert('/', (2, 9));

    result
}

struct CommandArguments<'a, 'c> {
    app: &'a mut MyApp,
    ctx: &'c egui::Context,
}

impl eframe::App for MyApp {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        let text_edit_id = egui::Id::new("text_edit");

        if let Some(ref error_message) = self.error_message {
            show_error_panel(ctx, error_message);
        }

        if is_first_frame(ctx) {
            ctx.memory_mut(|mem| mem.request_focus(text_edit_id));
        }

        let pressed_keys =
            if self.menu_stack.len() > 0 && ctx.memory(|mem| mem.has_focus(text_edit_id)) {
                consume_text_from_input(ctx)
            } else {
                String::new()
            };

        let esc_was_pressed =
            (!self.is_showing_menu() || self.search_state.is_some()) && consume_esc_press(ctx);
        let should_show_menu =
            !self.is_showing_menu() && (esc_was_pressed || alt_was_released_without_key_press(ctx));
        if should_show_menu {
            self.menu_stack.push(&MAIN_MENU);
        }

        if let Some(search_state) = &self.search_state {
            if consume_enter_press(ctx) {
                if let Ok(regex) = re::Regex::new(&search_state.pattern) {
                    match search_state.cursor_to_move {
                        CursorType::Both => {
                            self.buffer.cursor_mut().search(&regex);
                            self.buffer.set_secondary_cursor_to_primary_cursor();
                        }
                        CursorType::Secondary => {
                            self.buffer.secondary_cursor_mut().search(&regex);
                        }
                    }
                }
                ctx.memory_mut(|mem| mem.request_focus(text_edit_id));
                self.search_state = None;
            } else if esc_was_pressed {
                ctx.memory_mut(|mem| mem.request_focus(text_edit_id));
                self.search_state = None;
            }
        }

        let key_state = self.keys_to_menu_array(&pressed_keys);
        let mut active_menu_items: Vec<MenuItem> = vec![];
        let mut a_button_was_pressed = false;
        if let Some(&menu) = self.menu_stack.last() {
            let menu_appearance = menu.appearance(self, key_state);
            let menu_slots_activated_by_button =
                show_bottom_panel(ctx, self, Some(&menu_appearance));
            a_button_was_pressed = menu_slots_activated_by_button.any();

            active_menu_items =
                menu.items.select(menu_slots_activated_by_button).filter_map(|i| *i).collect();
            for key in pressed_keys.chars() {
                if let Some(item) = self.get_menu_item_from_key(key, menu) {
                    active_menu_items.push(item);
                }
            }
        } else {
            show_bottom_panel(ctx, self, None);
        }

        let response = egui::CentralPanel::default()
            .show(ctx, |ui| {
                ui.allocate_ui_with_layout(
                    ui.available_size(),
                    egui::Layout::centered_and_justified(egui::Direction::TopDown),
                    |ui| {
                        egui::ScrollArea::vertical()
                            .drag_to_scroll(false)
                            .show(ui, |ui| ui.add(self.make_text_edit(text_edit_id)))
                            .inner
                    },
                )
                .inner
            })
            .inner;
        if a_button_was_pressed {
            response.request_focus();
        }

        for active_menu_item in active_menu_items {
            (active_menu_item.callback)(&mut CommandArguments { app: self, ctx });
        }
    }
}

fn show_error_panel(ctx: &egui::Context, error_message: &str) {
    egui::TopBottomPanel::top("error message").show_separator_line(false).show(ctx, |ui| {
        ui.add_space(ui.style().spacing.window_margin.top);
        ui.colored_label(error_text_color(ui), error_message);
    });
}

fn error_text_color(ui: &egui::Ui) -> egui::Color32 {
    let luminance = luminance(text_color(ui)).max(20);

    // Reddish color with approximately the same luminance as the
    // default text color
    egui::Color32::from_rgb(luminance, luminance / 3, luminance / 3)
}

fn text_color(ui: &egui::Ui) -> egui::Color32 {
    ui.style()
        .visuals
        .override_text_color
        .unwrap_or(ui.style().visuals.widgets.inactive.fg_stroke.color)
}

fn luminance(color: egui::Color32) -> u8 {
    u8::try_from((u16::from(color.r()) + u16::from(color.g()) + u16::from(color.b())) / 3).unwrap()
}

impl MyApp {
    fn is_showing_menu(&self) -> bool {
        self.menu_stack.len() > 0
    }

    fn get_menu_item_from_key(&self, key: char, menu: &Menu) -> Option<MenuItem> {
        self.key_locations.get(&key).and_then(|(row, column)| menu.items[*row][*column])
    }

    fn keys_to_menu_array(&self, keys: &str) -> MenuArray<bool> {
        let mut result = MenuArray::default();
        for key in keys.chars() {
            if let Some(&(row_index, column_index)) = self.key_locations.get(&key) {
                result[row_index][column_index] = true;
            }
        }
        result
    }

    fn make_text_edit(&mut self, id: egui::Id) -> txted::TextEdit {
        let theme = &self.theme_set.themes["base16-eighties.dark"];
        txted::TextEdit::new(id, &mut self.buffer, &self.syntax_set, theme)
    }
}

fn show_bottom_panel(
    ctx: &egui::Context,
    app: &mut MyApp,
    menu_appearance: Option<&MenuAppearance>,
) -> MenuArray<bool> {
    let mut active_menu_items = Default::default();
    if let Some(menu_appearance) = menu_appearance {
        egui::TopBottomPanel::bottom("menu").show_separator_line(false).show(ctx, |ui| {
            active_menu_items = show_menu(ui, menu_appearance);
            ui.expand_to_include_y(
                ui.min_rect().bottom() + ui.style().spacing.window_margin.bottom,
            );
        });
    }
    egui::TopBottomPanel::bottom("status_bar").show(ctx, |ui| {
        ui.with_layout(egui::Layout::right_to_left(egui::Align::Center), |ui| {
            ui.label(format_cursor_position(app.buffer.cursor().position));
            if let Some(SearchState { pattern: ref mut search_pattern, .. }) = &mut app.search_state
            {
                ui.add_sized(
                    ui.available_size_before_wrap(),
                    egui::TextEdit::singleline(search_pattern).font(egui::TextStyle::Monospace),
                )
                .request_focus();
            }
        });
    });
    active_menu_items
}

fn format_cursor_position(position: buf::Position) -> String {
    format!("Ln. {}, Col. {}", position.line + 1, position.offset)
}

fn consume_text_from_input(ctx: &egui::Context) -> String {
    let mut text = String::new();
    ctx.input_mut(|input| {
        input.events.retain(|event| {
            if let egui::Event::Text(new_text) = event {
                text.push_str(new_text);
                false
            } else {
                true
            }
        });
    });
    text
}

fn consume_esc_press(ctx: &egui::Context) -> bool {
    ctx.input_mut(|input| input.consume_key(egui::Modifiers::NONE, egui::Key::Escape))
}

fn consume_enter_press(ctx: &egui::Context) -> bool {
    ctx.input_mut(|input| input.consume_key(egui::Modifiers::NONE, egui::Key::Enter))
}

// Return true if the Alt key was released and no non-modifier key was
// pressed while Alt was down.
fn alt_was_released_without_key_press(ctx: &egui::Context) -> bool {
    let alt_is_down = ctx.input(|input| input.modifiers.alt);
    let normal_key_is_pressed = ctx.input(|input| {
        input.events.iter().any(|event| matches!(event, egui::Event::Key { pressed: true, .. }))
    });
    let window_is_losing_focus = ctx.input(|input| {
        input.events.iter().any(|event| matches!(event, egui::Event::WindowFocused(false)))
    });
    ctx.data_mut(|data| {
        let alt_was_down = data.get_temp::<bool>("alt_was_down".into()).unwrap_or(false);
        data.insert_temp("alt_was_down".into(), alt_is_down);
        let alt_is_pressed = alt_is_down && !alt_was_down;
        let only_alt_has_been_down =
            data.get_temp::<bool>("only_alt_has_been_down".into()).unwrap_or(false);
        data.insert_temp(
            "only_alt_has_been_down".into(),
            (alt_is_pressed || only_alt_has_been_down)
                && alt_is_down
                && !normal_key_is_pressed
                && !window_is_losing_focus,
        );
        only_alt_has_been_down && !alt_is_down && !window_is_losing_focus
    })
}

fn is_first_frame(ctx: &egui::Context) -> bool {
    let not_first_frame_id = egui::Id::new("not_first_frame");
    let is_first_frame = ctx.data(|data| data.get_temp::<()>(not_first_frame_id).is_none());
    if is_first_frame {
        ctx.data_mut(|data| data.insert_temp(not_first_frame_id, ()));
    }
    is_first_frame
}

const MAIN_MENU: Menu = Menu {
    items: MenuArray::new([
        [
            Some(MENU_ITEM_FILE_MENU),
            Some(MENU_ITEM_CURSOR_LEFT_WORD),
            Some(MENU_ITEM_CURSOR_UP),
            Some(MENU_ITEM_CURSOR_RIGHT_WORD),
            Some(MENU_ITEM_DELETE),
            Some(MENU_ITEM_REDO),
            None,
            Some(MENU_ITEM_INSERT_NEW_LINE_ABOVE),
            None,
            None,
        ],
        [
            Some(MENU_ITEM_CURSOR_TO_LINE_START),
            Some(MENU_ITEM_CURSOR_LEFT),
            Some(MENU_ITEM_CURSOR_DOWN),
            Some(MENU_ITEM_CURSOR_RIGHT),
            Some(MENU_ITEM_CURSOR_TO_LINE_END),
            None,
            Some(MENU_ITEM_SELECTION_MENU),
            Some(MENU_ITEM_INSERT),
            None,
            None,
        ],
        [
            Some(MENU_ITEM_UNDO),
            Some(MENU_ITEM_CUT),
            Some(MENU_ITEM_COPY),
            Some(MENU_ITEM_PASTE),
            None,
            None,
            None,
            Some(MENU_ITEM_INSERT_NEW_LINE_BELOW),
            None,
            Some(MENU_ITEM_QUICK_SEARCH),
        ],
    ]),
};

const SELECTION_MENU: Menu = Menu {
    items: MenuArray::new([
        [
            None,
            Some(MENU_ITEM_SELECT_LEFT_WORD),
            Some(MENU_ITEM_SELECT_UP),
            Some(MENU_ITEM_SELECT_RIGHT_WORD),
            None,
            None,
            None,
            None,
            None,
            None,
        ],
        [
            Some(MENU_ITEM_SELECT_TO_LINE_START),
            Some(MENU_ITEM_SELECT_LEFT),
            Some(MENU_ITEM_SELECT_DOWN),
            Some(MENU_ITEM_SELECT_RIGHT),
            Some(MENU_ITEM_SELECT_TO_LINE_END),
            None,
            Some(MENU_ITEM_GO_BACK),
            None,
            None,
            None,
        ],
        [None, None, None, None, None, None, None, None, None, Some(MENU_ITEM_SELECT_QUICK_SEARCH)],
    ]),
};

const FILE_MENU: Menu = Menu {
    items: MenuArray::new([
        [
            Some(MENU_ITEM_GO_BACK),
            Some(MENU_ITEM_SAVE),
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
        ],
        [Some(MENU_ITEM_QUIT), None, None, None, None, None, None, None, None, None],
        [None, None, None, None, None, None, None, None, None, None],
    ]),
};

const MENU_ITEM_CURSOR_LEFT: MenuItem = MenuItem {
    label: "⬅",
    callback: |CommandArguments { app, .. }| {
        if !app.buffer.has_selection() {
            app.buffer.cursor_mut().move_left();
        }
        app.buffer.collapse_cursors_left();
    },
    is_enabled: Some(|app| {
        !app.buffer.cursor().is_at_start_of_line() || app.buffer.has_selection()
    }),
};

const MENU_ITEM_CURSOR_RIGHT: MenuItem = MenuItem {
    label: "➡",
    callback: |CommandArguments { app, .. }| {
        if !app.buffer.has_selection() {
            app.buffer.cursor_mut().move_right();
        }
        app.buffer.collapse_cursors_right();
    },
    is_enabled: Some(|app| !app.buffer.cursor().is_at_end_of_line() || app.buffer.has_selection()),
};

const MENU_ITEM_CURSOR_UP: MenuItem = MenuItem {
    label: "⬆",
    callback: |CommandArguments { app, .. }| {
        app.buffer.cursor_mut().move_up();
        app.buffer.secondary_cursor_mut().position = app.buffer.cursor().position;
    },
    is_enabled: Some(|app| !app.buffer.cursor().is_on_first_line() || app.buffer.has_selection()),
};

const MENU_ITEM_CURSOR_DOWN: MenuItem = MenuItem {
    label: "⬇",
    callback: |CommandArguments { app, .. }| {
        app.buffer.cursor_mut().move_down();
        app.buffer.secondary_cursor_mut().position = app.buffer.cursor().position;
    },
    is_enabled: Some(|app| !app.buffer.cursor().is_on_last_line() || app.buffer.has_selection()),
};

const MENU_ITEM_CURSOR_LEFT_WORD: MenuItem = MenuItem {
    label: "⬅ Word",
    callback: |CommandArguments { app, .. }| {
        app.buffer.cursor_mut().move_to_previous_word();
        *app.buffer.secondary_cursor_mut() = app.buffer.cursor().clone();
    },
    is_enabled: Some(|app| {
        !app.buffer.cursor().is_at_start_of_line() || app.buffer.has_selection()
    }),
};

const MENU_ITEM_CURSOR_RIGHT_WORD: MenuItem = MenuItem {
    label: "➡ Word",
    callback: |CommandArguments { app, .. }| {
        app.buffer.cursor_mut().move_to_next_word();
        *app.buffer.secondary_cursor_mut() = app.buffer.cursor().clone();
    },
    is_enabled: Some(|app| !app.buffer.cursor().is_at_end_of_line() || app.buffer.has_selection()),
};

const MENU_ITEM_CURSOR_TO_LINE_START: MenuItem = MenuItem {
    label: "⏮ Home",
    callback: |CommandArguments { app, .. }| {
        let offset_of_first_non_blank = app.buffer.offset_of_first_non_blank_on_current_line();
        app.buffer.cursor_mut().position.offset =
            if app.buffer.cursor().position.offset != offset_of_first_non_blank {
                offset_of_first_non_blank
            } else {
                0
            };
        app.buffer.secondary_cursor_mut().position = app.buffer.cursor().position;
    },
    is_enabled: None,
};

const MENU_ITEM_CURSOR_TO_LINE_END: MenuItem = MenuItem {
    label: "⏭ End",
    callback: |CommandArguments { app, .. }| {
        app.buffer.cursor_mut().move_to_end_of_line();
        app.buffer.collapse_cursors_right();
    },
    is_enabled: Some(|app| !app.buffer.cursor().is_at_end_of_line() || app.buffer.has_selection()),
};

const MENU_ITEM_DELETE: MenuItem = MenuItem {
    label: "🗑 Delete",
    callback: |CommandArguments { app, .. }| {
        if app.buffer.has_selection() {
            app.buffer.delete_selection();
        } else {
            app.buffer.delete();
        }
    },
    is_enabled: Some(|app| app.buffer.has_selection() || !app.buffer.cursor().is_at_end_of_line()),
};

const MENU_ITEM_INSERT: MenuItem =
    MenuItem { label: "Insert", callback: |args| args.app.menu_stack = vec![], is_enabled: None };

const MENU_ITEM_INSERT_NEW_LINE_ABOVE: MenuItem = MenuItem {
    label: "Insert ⬆",
    callback: |CommandArguments { app, .. }| {
        app.buffer.cursor_mut().move_to_start_of_line();
        app.buffer.secondary_cursor_mut().move_to_start_of_line();
        app.buffer.insert('\n');
        app.menu_stack = vec![];
    },
    is_enabled: None,
};

const MENU_ITEM_INSERT_NEW_LINE_BELOW: MenuItem = MenuItem {
    label: "Insert ⬇",
    callback: |CommandArguments { app, .. }| {
        app.buffer.cursor_mut().move_to_end_of_line();
        app.buffer.secondary_cursor_mut().move_to_end_of_line();
        app.buffer.insert('\n');
        app.buffer.cursor_mut().move_down();
        app.buffer.secondary_cursor_mut().move_down();
        app.menu_stack = vec![];
    },
    is_enabled: None,
};

const MENU_ITEM_UNDO: MenuItem =
    MenuItem { label: "⮪ Undo", callback: |args| args.app.buffer.undo(), is_enabled: None };

const MENU_ITEM_REDO: MenuItem =
    MenuItem { label: "⮫ Redo", callback: |args| args.app.buffer.redo(), is_enabled: None };

const MENU_ITEM_CUT: MenuItem = MenuItem {
    label: "✂ Cut",
    callback: |args| {
        args.app.buffer.start_operation_group();
        (MENU_ITEM_COPY.callback)(args);
        (MENU_ITEM_DELETE.callback)(args);
        args.app.buffer.end_operation_group();
    },
    is_enabled: Some(|app| app.buffer.has_selection()),
};

const MENU_ITEM_COPY: MenuItem = MenuItem {
    label: "🗐 Copy",
    callback: |CommandArguments { app, ctx, .. }| {
        if let Some(text_to_copy) = app.buffer.read_selection() {
            ctx.copy_text(text_to_copy);
        }
    },
    is_enabled: Some(|app| app.buffer.has_selection()),
};

const MENU_ITEM_PASTE: MenuItem = MenuItem {
    label: "📋 Paste",
    callback: |CommandArguments { app, .. }| {
        if let Some(text_to_paste) = app.clipboard.get() {
            app.buffer.start_operation_group();
            if app.buffer.has_selection() {
                app.buffer.delete_selection();
            }
            app.buffer.insert(&text_to_paste);
            let end_of_pasted_text =
                app.buffer.cursor().position + buf::Distance::of_str(&text_to_paste);
            app.buffer.set_both_cursors_to_position(end_of_pasted_text);
            app.buffer.end_operation_group();
        }
    },
    is_enabled: None,
};

const MENU_ITEM_QUIT: MenuItem = MenuItem {
    label: "Quit",
    callback: |args| args.ctx.send_viewport_cmd(egui::viewport::ViewportCommand::Close),
    is_enabled: None,
};

const MENU_ITEM_SELECTION_MENU: MenuItem = MenuItem {
    label: "Select ⏵",
    callback: |args| args.app.menu_stack.push(&SELECTION_MENU),
    is_enabled: None,
};

const MENU_ITEM_SELECT_LEFT: MenuItem = MenuItem {
    label: "Select ⬅",
    callback: |args| args.app.buffer.secondary_cursor_mut().move_left(),
    is_enabled: Some(|app| !app.buffer.secondary_cursor().is_at_start_of_line()),
};

const MENU_ITEM_SELECT_RIGHT: MenuItem = MenuItem {
    label: "Select ➡",
    callback: |args| args.app.buffer.secondary_cursor_mut().move_right(),
    is_enabled: Some(|app| !app.buffer.secondary_cursor().is_at_end_of_line()),
};

const MENU_ITEM_SELECT_UP: MenuItem = MenuItem {
    label: "Select ⬆",
    callback: |args| args.app.buffer.secondary_cursor_mut().move_up(),
    is_enabled: Some(|app| !app.buffer.secondary_cursor().is_on_first_line()),
};

const MENU_ITEM_SELECT_DOWN: MenuItem = MenuItem {
    label: "Select ⬇",
    callback: |args| args.app.buffer.secondary_cursor_mut().move_down(),
    is_enabled: Some(|app| !app.buffer.secondary_cursor().is_on_last_line()),
};

const MENU_ITEM_SELECT_TO_LINE_START: MenuItem = MenuItem {
    label: "Select ⏮",
    callback: |args| args.app.buffer.secondary_cursor_mut().move_to_start_of_line(),
    is_enabled: None,
};

const MENU_ITEM_SELECT_TO_LINE_END: MenuItem = MenuItem {
    label: "Select ⏭",
    callback: |args| args.app.buffer.secondary_cursor_mut().move_to_end_of_line(),
    is_enabled: None,
};

const MENU_ITEM_SELECT_LEFT_WORD: MenuItem = MenuItem {
    label: "Select ⬅ Word",
    callback: |CommandArguments { app, .. }| {
        app.buffer.secondary_cursor_mut().move_to_previous_word()
    },
    is_enabled: Some(|app| !app.buffer.secondary_cursor().is_at_start_of_line()),
};

const MENU_ITEM_SELECT_RIGHT_WORD: MenuItem = MenuItem {
    label: "Select ➡ Word",
    callback: |CommandArguments { app, .. }| app.buffer.secondary_cursor_mut().move_to_next_word(),
    is_enabled: Some(|app| !app.buffer.secondary_cursor().is_at_end_of_line()),
};

const MENU_ITEM_SELECT_QUICK_SEARCH: MenuItem = MenuItem {
    label: "🔍 Quick Search",
    callback: |CommandArguments { app, .. }| {
        app.search_state =
            Some(SearchState { pattern: String::new(), cursor_to_move: CursorType::Secondary })
    },
    is_enabled: None,
};

const MENU_ITEM_GO_BACK: MenuItem = MenuItem {
    label: "⏴ Back",
    callback: |CommandArguments { app, .. }| {
        app.menu_stack.pop();
    },
    is_enabled: None,
};

const MENU_ITEM_FILE_MENU: MenuItem = MenuItem {
    label: "File ⏵",
    callback: |CommandArguments { app, .. }| app.menu_stack.push(&FILE_MENU),
    is_enabled: None,
};

const MENU_ITEM_SAVE: MenuItem = MenuItem {
    label: "\u{1F4BE} Save",
    callback: |CommandArguments { app, .. }| {
        if let Err(error) = app.buffer.save() {
            app.error_message =
                if let Some(path) = app.buffer.path().as_ref().and_then(|p| p.to_str()) {
                    Some(format!("Unable to save {}: {}", path, error.to_string()))
                } else {
                    Some(format!("Unable to save: {}", error.to_string()))
                };
        }
    },
    is_enabled: Some(|app| app.buffer.modified()),
};

const MENU_ITEM_QUICK_SEARCH: MenuItem = MenuItem {
    label: "🔍 Quick Search",
    callback: |CommandArguments { app, .. }| {
        app.search_state = Some(SearchState {
            pattern: app.buffer.read_selection().unwrap_or_else(String::new),
            cursor_to_move: CursorType::Both,
        });
    },
    is_enabled: None,
};

const KEY_OFFSETS: &[f32] = &[0.0, 0.25, 0.75];

fn show_menu(ui: &mut egui::Ui, menu: &MenuAppearance) -> MenuArray<bool> {
    let mut active_menu_items: MenuArray<bool> = MenuArray::default();

    let orig_item_spacing = ui.style().spacing.item_spacing;
    let equal_item_spacing = orig_item_spacing.x.min(orig_item_spacing.y);

    let custom_style = {
        let mut style = (**ui.style()).clone();
        // Make spacing equal in x and y -- this makes the keyboard
        // layout look better
        style.spacing.item_spacing = egui::Vec2::new(equal_item_spacing, equal_item_spacing);
        Arc::new(style)
    };
    ui.set_style(custom_style.clone());

    let available_rect = ui.available_rect_before_wrap();
    let button_size = menu_button_size(ui, available_rect);

    for (row_index, row) in menu.rows().enumerate() {
        ui.horizontal(|ui| {
            ui.add_space(KEY_OFFSETS[row_index] * (button_size.x + equal_item_spacing));
            show_menu_row(ui, row_index, row, button_size, &custom_style, &mut active_menu_items);
        });
    }

    active_menu_items
}

fn menu_button_size(ui: &egui::Ui, menu_rect: egui::Rect) -> egui::Vec2 {
    const MAX_KEY_OFFSET: f32 = KEY_OFFSETS[2];

    let width = menu_rect.width();
    let keys_per_row_f: f32 = u16::try_from(MENU_COLUMNS).unwrap().into();

    let item_spacing = ui.spacing().item_spacing.x;
    let total_item_spacing_x = (keys_per_row_f - 1.0 - MAX_KEY_OFFSET) * item_spacing;
    let total_button_width = width - total_item_spacing_x - ui.spacing().window_margin.right;

    egui::vec2(
        total_button_width / (keys_per_row_f + MAX_KEY_OFFSET),
        2.0 * ui.spacing().interact_size.y,
    )
}

fn show_menu_row(
    ui: &mut egui::Ui,
    row_index: usize,
    row: &[Option<MenuItemAppearance>],
    button_size: egui::Vec2,
    style: &Arc<egui::Style>,
    active_menu_items: &mut MenuArray<bool>,
) {
    for (column_index, menu_item) in row.iter().enumerate() {
        ui.scope(|ui| {
            if let Some(menu_item) = menu_item {
                if row_index == 1 && (column_index == 3 || column_index == 6) {
                    emphasize_visuals(ui.style_mut());
                }

                if animate_key_press(
                    ui.ctx(),
                    menu_item.is_activated && menu_item.is_enabled,
                    row_index,
                    column_index,
                ) {
                    // Make inactive buttons look active
                    ui.visuals_mut().widgets.inactive = ui.visuals().widgets.active;
                }

                let response = ui
                    .add_enabled_ui(menu_item.is_enabled, |ui| {
                        ui.add_sized(button_size, egui::Button::new(menu_item.label).wrap(true))
                    })
                    .inner;

                ui.set_style(style.clone());

                if response.clicked() {
                    active_menu_items[row_index][column_index] = true;
                }
            } else {
                ui.add_enabled_ui(false, |ui| ui.add_sized(button_size, egui::Button::new("")));
            }
        });
    }
}

// Return true if the menu button at (row, column) should appear active
// due to a key press.  is_activated tells whether the key has been
// pressed on the current frame.
fn animate_key_press(ctx: &egui::Context, is_activated: bool, row: usize, column: usize) -> bool {
    let id = egui::Id::new("animate_key_press").with(row).with(column);
    ctx.animate_bool_pulse(id, is_activated)
}

fn emphasize_visuals(style: &mut egui::Style) {
    let widgets = &mut style.visuals.widgets;
    // This somehow doesn’t affect the styles of noninteractive buttons
    for widget_visuals in [
        &mut widgets.noninteractive,
        &mut widgets.inactive,
        &mut widgets.hovered,
        &mut widgets.active,
        &mut widgets.open,
    ] {
        let rounding = &mut widget_visuals.rounding;
        for corner in [&mut rounding.nw, &mut rounding.ne, &mut rounding.sw, &mut rounding.se] {
            // This will not work if the original style has very rounded corners
            *corner += 0.5 * style.spacing.interact_size.y;
        }
    }
}

struct Menu {
    items: MenuArray<Option<MenuItem>>,
}

impl Menu {
    fn appearance(&self, app: &MyApp, key_state: MenuArray<bool>) -> MenuAppearance {
        self.items.as_ref().zip(key_state).map(|(menu_item, is_activated)| {
            menu_item.map(|item| item.appearance(app, is_activated))
        })
    }
}

#[derive(Copy, Clone)]
struct MenuItem {
    label: &'static str,
    callback: fn(&mut CommandArguments),
    is_enabled: Option<fn(&MyApp) -> bool>,
}

impl MenuItem {
    fn is_enabled(&self, app: &MyApp) -> bool {
        self.is_enabled.map(|f| f(app)).unwrap_or(true)
    }

    fn appearance(&self, app: &MyApp, is_activated: bool) -> MenuItemAppearance {
        MenuItemAppearance { label: self.label, is_activated, is_enabled: self.is_enabled(app) }
    }
}

type MenuAppearance = MenuArray<Option<MenuItemAppearance>>;

struct MenuItemAppearance {
    label: &'static str,
    is_activated: bool,
    is_enabled: bool,
}
